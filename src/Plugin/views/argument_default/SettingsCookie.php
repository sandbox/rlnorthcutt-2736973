<?php

namespace Drupal\views_cookie\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;

/**
 * The fixed argument default handler.
 *
 * @ingroup views_argument_default_plugins
 *
 * @ViewsArgumentDefault(
 *   id = "settings_cookie",
 *   title = @Translation("Cookie from Settings")
 * )
 */
class SettingsCookie extends Cookie implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['argument'] = array('default' => '');

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['argument'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the cookie from the settings to use'),
      '#default_value' => $this->options['argument'],
      '#options' => $this->settingsCookieList(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {}

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {}

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {}

  /**
   * {@inheritdoc}
   */
  protected function getCookieName() {}

  /**
   * Returns an array of cookie names based on the settings.
   */
  protected function settingsCookieList() {
    $options = [];
    // Get the settings.
    // @TODO: Find a way to grab the exposed filter form ids also (if set)
    $config = \Drupal::config('views.cookie.settings');
    $vocabularies = array_filter($config->get('cookie_selected_vocabularies'));
    foreach ($vocabularies as $vocab) {
      $options['vocabulary_' . $vocab] = 'Vocabulary : ' . $vocab;
    }
    $types = array_filter($config->get('cookie_selected_node_types'));
    foreach ($types as $type) {
      $options['node_type_' . $type] = 'Content type : ' . $type;
    }
    return $options;
  }

}
