<?php

namespace Drupal\views_cookie\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;

/**
 * The fixed argument default handler.
 *
 * @ingroup views_argument_default_plugins
 *
 * @ViewsArgumentDefault(
 *   id = "cookie",
 *   title = @Translation("Cookie")
 * )
 */
class Cookie extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['argument'] = array('default' => '');

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['argument'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of cookie to pull from'),
      '#default_value' => $this->options['argument'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $name = $this->getCookieName();
    return \Drupal::requestStack()->getCurrentRequest()->cookies->get($name);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $name = $this->getCookieName();
    return ['cookies:' . $name];
  }

  /**
   * Helper function to get the cookie name. We are abstracting this out to make
   * it easier to extend this in the future.
   */
  protected function getCookieName() {
    return $this->options['argument'];
  }

}
