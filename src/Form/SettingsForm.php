<?php

namespace Drupal\views_cookie\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\node\Entity\NodeType;

/**
 * Form builder for the advanced admin settings page.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'views_cookie_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['views.cookie.settings'];
  }

  // @TODO : Change vocabularies & node types to plugins so other modules can add more

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('views.cookie.settings');
    $form = parent::buildForm($form, $form_state);

    $form['cookie_settings'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-taxonomy',
      '#attached' => [
        'library' => [
          'views_cookie/admin_form_summaries',
        ],
      ],
    ];

    $form['taxonomy'] = [
      '#type' => 'details',
      '#title' => $this->t('Taxonomy'),
      '#group' => 'cookie_settings'
    ];
    $form['taxonomy']['selected_vocabularies'] = [
      '#title' => $this->t('Selected vocabularies'),
      '#default_value' => array_filter($config->get('cookie_selected_vocabularies')),
      '#options' => $this->vocabulariesList(),
      '#type' => 'checkboxes',
      '#description' => $this->t('Select the vocabularies to automatically create cookies for. This will create a cookie of the vocabulary name with the value of the most recent term the user has viewed content for.'),
    ];

    $form['types'] = [
      '#type' => 'details',
      '#title' => $this->t('Content types'),
      '#group' => 'cookie_settings',
    ];
    $form['types']['selected_node_types'] = [
      '#title' => $this->t('Selected types'),
      '#default_value' => array_filter($config->get('cookie_selected_node_types')),
      '#options' => $this->contentTypeList(),
      '#type' => 'checkboxes',
      '#description' => $this->t('Select the content types to automatically create cookies for. This will create a cookie of the type name with the value of the most recent id the user has viewed of that type.'),
    ];

    $form['filters'] = [
      '#type' => 'details',
      '#title' => $this->t('Exposed filters'),
      '#group' => 'cookie_settings',
    ];
    $form['filters']['all_filters'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add a cookie for each views exposed filter'),
      '#description' => $this->t("All views exposed filters will create a cookie storing the value of the input with the name of the key."),
      '#default_value' => $config->get('cookie_all_filters'),
    ];

    $form['days'] = [
      '#type' => 'details',
      '#title' => $this->t('Cookie life'),
      '#group' => 'cookie_settings',
    ];
    $form['days']['lifespan'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Set how long the cookies should live.'),
      '#description' => $this->t("All the cookies created by the settings will have an expiration date for this many days into the future."),
      '#default_value' => $config->get('cookie_lifespan') ?: 180,
      '#field_suffix' => $this->t(' days'),
      '#size' => 10,
      '#maxlength' => 8,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('views.cookie.settings')
      ->set('cookie_selected_vocabularies', $form_state->getValue('selected_vocabularies', []))
      ->set('cookie_selected_node_types', $form_state->getValue('selected_node_types', []))
      ->set('cookie_all_filters', $form_state->getValue('all_filters'))
      ->set('cookie_lifespan', $form_state->getValue('lifespan'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Returns an array of vocabulary names for the options list in the form.
   */
  protected function vocabulariesList() {
    $options = [];
    $vocabularies = Vocabulary::loadMultiple();
    foreach ($vocabularies as $vid => $values) {
      $options[$vid] = $values->label();
    }
    return $options;
  }

  /**
   * Returns an array of content type names for the options list in the form.
   */
  protected function contentTypeList() {
    $options = [];
    $content_types = NodeType::loadMultiple();
    foreach ($content_types as $type) {
      $options[$type->id()] = $type->label();
    }
    return $options;
  }

}
