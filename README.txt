Install and enable as normal.

Configure the settings from the Views settings in the admin section.

To use, add a contextual filter and use the "Cookie" type of default value. Put in the name of the cookie you want to switch with, and it will work.

You can also optionally use the "Cookie from Settings" default value. This will provide a list of content types and/or taxonomy vocabularies that have been configured in the settings.

TODO:
- see code comments
- create some tests
- see about extending to panels
