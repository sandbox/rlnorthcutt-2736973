(function ($, Drupal) {

    'use strict';

    /**
     * Provides the summaries for the vertical tabs in the Views Cookie settings form.
     *
     * @type {Drupal~behavior}
     *
     * @prop {Drupal~behaviorAttach} attach
     */
    Drupal.behaviors.viewsCookieSettingsSummary = {
        attach: function () {
            $('[data-drupal-selector="edit-taxonomy"]').drupalSetSummary(function () {
                return checklistSummary('selected-vocabularies');
            });

            $('[data-drupal-selector="edit-types"]').drupalSetSummary(function () {
                return checklistSummary('selected-node-types');
            });

            $('[data-drupal-selector="edit-filters"]').drupalSetSummary(function () {
                return document.querySelector('input[name="all_filters"]').checked ? Drupal.t('Enabled') : Drupal.t('Disabled');
            });

            $('[data-drupal-selector="edit-days"]').drupalSetSummary(function () {
                return document.querySelector('input[name="lifespan"]').value + ' days' || Drupal.t('None');
            });
        }
    };

    function checklistSummary(name) {
        var selected = '';
        $('#edit-' + name + ' :checked').each(function () {
            selected = selected ? selected + ', ' : '';
            selected += $("label[for='" + this.id + "']").text();
        });
        return Drupal.t('!selected', {'!selected': selected ? selected : Drupal.t('None selected.')});
    }

})(jQuery, Drupal);
