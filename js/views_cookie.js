/**
 * @file
 * Sets the cookies on the client side for the default views cookies config.
 */

(function ($) {
  'use strict';

  // Create a cookie to store the value of any views exposed filter when it is set.
  Drupal.behaviors.views_cookie_exposed_filters = {
    attach: function (context, settings) {
      // @TODO: need to get once() to run here too
      if (drupalSettings.viewsCookie.exposedFilters) {
        $('form.views-exposed-form').on('change', function () {
          $('form input[type=text], select').each(function () {
            var name = $(this).attr('name');
            var value = $(this).val();
            var days = drupalSettings.viewsCookie.cookieLifespan;
            vcCreateCookie(name, value, days);
          });
          return false;
        });
      }
    }
  };

  // Create cookies to store the node specific values sent by PHP.
  Drupal.behaviors.views_cookie_node_values = {
    attach: function (context, settings) {
      var days = drupalSettings.viewsCookie.cookieLifespan;
      $.each(drupalSettings.viewsCookie.nodeValues, function (name, value) {
        // @TODO: need to figure out how to get once() to run here too
        vcCreateCookie(name, value, days);
      });
    }
  };

  // Helper functions.
  function vcCreateCookie(name, value, days) {
    var expires = '';
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = '; expires=' + date.toGMTString();
    }
    document.cookie = name + '=' + value + expires + '; path=/';
  }

}(jQuery));
